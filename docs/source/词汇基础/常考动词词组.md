# 常考动词词组

**Unit 1**

`account for`  说明，解释
`accustom to`  习惯于
`accuse sb.of sth.` 因某事指控某人
`amaze at`  使惊奇，使惊愕
`be about to`  将要(做某事)
`be able  to`  能够(做某事)



```
1. You will not be able  ____  pass the examination unless you work harder than you do now.
   
2. The plane is just ____ to start.

3. When he was set free after twenty years in prison, he was amazed ____ the changes he found.

4. The customer accused the cook ____ using canned potatoes.
   
5. He has been asked to account his plan.

6. He finds it difficult to accustom himself  the climate here.

(1. to       2. about        3.at      4.of       5. for     6. to)
```

---



**Unit 2**



`break away`    脱离，逃跑
`break down  `   损环:分解，瓦解(精神、健康）；垮下来
`break in  `      强行闯人，打断
`break off`    中止，中断
`break out`    爆发；突然出现
`break up`   驱散，散开
`break through`   突围，突破
`bring about`   导致，引起
`bring down`   减少，降低
`bring up`   教育，培养;长大;提出
`burn out   `   烧光，烧毁
`burn up`    烧起来，烧光，烧掉



```
1. He was living in France when the war broke _____.

2. Science and technology have brought _____ great social changes.

3. Can't you break _____ from old habits?

4. His parents died when he was young, so he was_____by his aunt.

5. The police came and______the demonstration.

6. Shopkeepers have been asked to_____their prices down.

(1. out  2. about 3. away 4. brought up  5. broke up 6. bring)
```

