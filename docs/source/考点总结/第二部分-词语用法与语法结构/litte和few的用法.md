```
33.Hurry up ! We have______to spare.
A.a ltte time
B.a few time
C.few time
D.lttle time

[答案] D。
考查ltte和few的用法。
few与lttle作形容词时都表示“几乎没有”，相当于否定词，
不同的是few修饰可数名词的复数形式，ltte 修饰不可数名词。 
a few表示“一些，几个”，修饰可数名词复数形式
，而a ltte为“一点儿，少量”，修饰不可数名词time是不可数名词，
故排除B、C。
句意为:快点!我们几乎没有多余的时间。lttle 有否定含义，故选D。
```

