```
[2019.11]
19. Young_______he is, he knows what is the right thing to do.
A.that
B.as
C although
D.however

19.[答案]B。
考查让步状语从句。
as放在形容词之后，引导让步状语从句,意思是“尽管,即使”，此处应使用部分倒装结构。
这句话的意思是:虽然他还年轻,但他知道什么是正确的事。故选B。
```

```
[2019.5]
38. No matter how_____,it is not necessarily lifeless.
A. a desert may be dry
B. dry may a desert be
C. may a desert be dry
D. dry a desert may be


[答案] D
[译文]不管沙漠有多干燥,也不一定是没有生命的。
[解析]本题考查no matter 的用法。
no matter 引导让步状语从句,从句的引导词只能是疑问词，在本题中,疑问词为how。且从句的语序是“疑问词+陈述句”,故选D。
no matter how + 形容词 + 主语 + 动词，主句。这里 a desert是主语，dry干燥的，是形容词。
```

