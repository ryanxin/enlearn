```
17. The flowers in the garden need ________badly; otherwise they will dry up soon.
A. watering
B. to water
C. watered
D. to have watered

[答案] A。

考查need的用法。
need作实义动词时，need doing通常表示被动意义，即“需要被....”
，而need to do则意为“需要做...

本句意为:花园里的花急需浇水，否则它们会很快干枯
故选A。
```



```
```

