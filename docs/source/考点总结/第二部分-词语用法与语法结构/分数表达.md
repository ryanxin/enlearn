# 分数表达





```
[2021年05月] 
41. Among these foreign students ,_____of them are from English-speaking countries.
A. two third
B. two three
C. two thirds
D. two threes


[答案]C
[译文]在这些外国学生中,有三分之二来自英语国家。
[解析]本题考查分数表达。分数是由基数词和序数词一起来表示的。
基数词做分子,序数词作分母,分子大于“1”，序数词都要用复数形式。
根据语法,故选C。thirds三分之一
```

