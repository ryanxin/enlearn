# 限定词



```
[2019.11]
41.- How______money do I need to take ?
一A About three hundred dollars.
A.more
B. much
C many
D.few .

[答案]B。
考查限定词。
much修饰不可数名词，many修饰可数名词，意为“很多”，
how many/much意为“多少”; 
more 是many和much的比较级形式，表示“更多的”: few 修饰可数名词，表示“很少，几乎
没有”。本句中的money为不可数名词，句意为:我需要带多少钱?大约300美元。故选B。


```

