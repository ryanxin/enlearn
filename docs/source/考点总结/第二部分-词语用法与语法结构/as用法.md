```
[2019.5]
18.______he walked, he looked carfully at the ice in front of him.
A. If
B. As
C. Since
D. Whether

[答案] B
[译文]他一边走，一边仔细地看着面前的冰。
[解析]本题考查as的用法。as作连词引导时间状语从句,表示“当...的时候”，有“随.....之意，
与while意义相近，强调两个动作同时发生;或某事一发生, 另一事立即发生,故选B。
```

