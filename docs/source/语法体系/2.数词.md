# 数词



## 1.基数词
个位数 (0~9 )

| 零   | 一   | 二   | 三    | 四   | 五   | 六   | 七    | 八    | 九   |
| ---- | ---- | ---- | ----- | ---- | ---- | ---- | ----- | ----- | ---- |
| zero | one  | two  | three | four | five | six  | seven | eight | nine |



十到十九(10~19 )



| 十   | 十一   | 十二   | 十三     | 十四     | 十五    | 十六    | 十七      | 十八     | 十九     |
| ---- | ------ | ------ | -------- | -------- | ------- | ------- | --------- | -------- | -------- |
| ten  | eleven | twelve | thirteen | fourteen | fifteen | sixteen | seventeen | eighteen | nineteen |



十位数，二十到九十（20~90）

| 二十   | 三十   | 四十  | 五十  | 六十  | 七十    | 八十   | 九十   |
| ------ | ------ | ----- | ----- | ----- | ------- | ------ | ------ |
| twenty | thirty | forty | fifty | sixty | seventy | eighty | ninety |



二十以上，几十几 （21~99）

> 十位用几十，个位用个位

twenty  one  二十一

ninety nine 九十九



### 数字表达规律

中文划分规律：`10,0000,0000`

英文划分规律：`1,000,000,000`





| 1       | 0                   | 0           | 0       | 0                    | 0            | 0        | 0       | 0    | 0    |
| ------- | ------------------- | ----------- | ------- | -------------------- | ------------ | -------- | ------- | ---- | ---- |
| 十亿    | 亿                  | 千万        | 百万    | 十万                 | 万           | 千       | 百      | 十   | 个   |
| 1       | 0                   | 0           | 0       | 0                    | 0            | 0        | 0       | 0    | 0    |
| billion | one hundred million | ten million | million | one hundred thousand | ten thousand | thousand | hundred | ten  | one  |



- 从右往左，每三位加一个逗号，倒数第一个逗号之前是thousand（千）,倒数第二个逗
  号之前是million（百万）,倒数第三个逗号之前是billion,倒数第四个逗号之前是tillion（万亿）。
- 逗号之间的百位和十位要加and,数字隔零要加and



<br>

### 一百到九百九十九（100-999）



```
one hundred  一 百(100)

one hundred and one 一 百零(101 )

nine hundred and ninety-nine九百九十九(999)
```

<br>

### 一千到九千九百九十九（1,000-9,999）

```
one thousand 一千(1,000)

one thousand and one 一千零一(1,001 )

one thousand and twenty 一千零二十(1,020) 

nine thousand nine hundred and ninety-nine 九千九百九十九(9,999)
```

<br>



### 一万到一万零九百九十九（10,000-10,999）

```
一万(10*1000=1 0,000 ) 英语中没有万这个词，用10个千表示。

ten thousand 一万(10,000 )

ten thousand and one 一万零一(10,001 )

ten thousand and twenty 一万零二十(10,020) 

ten thousand and two hundred 一万零二百(10,200)

ten thousand nine hundred and ninety-nine 一万零九百九十九(10,999)
```

<br>



### 一万一千到九万九千九百九十九（11,000~99,999）

```
与一万同理11个千

eleven thousand 一万一千(11*1000=11,000 )

eleven thousand and one 一万一千零一(11,001 )

eleven thousand and twenty 一万一千零二十(11,020)

eleven thousand two hundred 一万一千零二百(11,200)

ninety nine thousand nine hundred and ninety-nine 九万九千九百九十九(99,999)
```

<br>

### 十万到九十九万九千九百九十九( 100,000~999,999 )

```
十万( 100*1000=100,000)
one hundred thousand 十万（100,000）

one hundred thousand and one 十万零一 (100,001 )

one hundred thousand and twenty 十万零二十 (100,020)

one hundred thousand and two hundred 十万零二百(100,200)

one hundred and two thousand and one 十万零二千零一(102,001 )

nine hundred and ninety-nine thousand nine hundred and ninety-nine 九十九万九千九百九十九（999,999）
```



<br>

### 一百万到九千九百九十九万九千九百九十九 (1 ,000,000-99,999,999)
```

one million 一百万(1 ,000,000)

one million and one 一百万零一(1,000,001 )

one million and one thousand 一百万零一千(1 ,001 ,000) 

one million and ten thousand 一百万零一万(1 ,010,000)

Ninety-nine million nine hundred and ninety-nine thousand nine hundred and ninety-nine 九千九百九十九万九千九百九十九(99,999,999)
```



### 一亿到十亿以及更多 ( 100,000,000~1 ,000,000,000~∞)

```

one hundred million 一亿(100,000,000 )

one hundred million and one 一亿零一(100,000,001 )

one hundred million and one thousand 一亿零千(100,001 ,000 )

one hundred and one million 一亿零一百万(101 ,000,000 )

one billion 十亿( 1 ,000,000.000 )

Thirty-one bllion twenty million nine hundred thousand eight hundred and eighty-eight 三百一十亿零两千九十万零八百八十八(31,020,900,888)
```

<br>

### 基数词使用特点



- 作数词时候，前面可以加数词，后面不能加s，two thousand, three
  thousands ( 错)

- 如果数词用作名词时候，意思是好几..的时候，前面可以加some、
  many、several这种词或者加数词，后面可以加of

- hundreds of ( 好几百) five hundreds of ( 五百左右)
  many millions of ( 数百万)



```
表示年龄
I'm twenty (我20岁)
```



```
表示编号
Room 101=Room one 0 one(101房间)
Bus No.10=Bus Number ten ( 10路公交车)
```

```
表示年代
1940's=nineteen forty's (20世纪40年代)
```

```
表示年份
2008=twenty thousand and eight
1949=nineteen forty-nine 
2019.9.10=Sep.10th,2019=September,tenth,twenty nineteen
```

<br>

<br>

### 基数词时间用法



表示时刻(中国人描述时间习惯精确到个位分钟，外国人描述时间习惯精确到十位分钟，几分钟一般省略)

```
6: 00=six o'clock=six on the dot (6点整)
6: 05=six 0 five=five past six ( 6点05分，6点过5分)
6: 15=six fifteen=a quarter past six (6点15分，6点过一刻)
6: 30=six thirty=half past six ( 6点30分，6点半)
6: 45=six forty-five=a quarter to (ill) seven ( 6点45分，7点差一刻)
6: 55=six fifty-five=five to (till) seven ( 6点55分，7点差5分)

```

<br>



- 时区: time zones

- 上午: ante meridiem=a.m.

- 下午: post meridiem=p.m.



24小时制=24-hour clock=military time (北美不太使用24小时制，都是说12小时制加am和pm )
21: 00=twenty one hours 
21: 20=twenty one twenty

<br>

<br>

### 基数词表示电话号码



电话号码直接读数字，北美电话，区号(3位) +用户号(7位)

```
647-300-2516=six four seven-three double o-two five one six
```




说价格(经常省略dollars和cents)

```
nine thirty= nine dollars and thirty cents (9.30$)
ninety-nine ninety-eight= ninety-nine dollars and ninety-eight cents (99.98$)
```




表示小数( 经常省略zero,或者读成o )

```
0.5= (zero) point five (零点五)
0.05= (zero) point o five (零点零五)
```




两个一样数字，三个-样数字( double、triple )

```
007=double o seven ( 零零七) 
boeing 777=boeing triple seven (波音777)
```



<br>

<br>



## 2.序数词



### **第几(第零到第九)**

| 第零   | 第一  | 第二   | 第三  | 第四   | 第五  | 第六  | 第七    | 第八   | 第九  |
| ------ | ----- | ------ | ----- | ------ | ----- | ----- | ------- | ------ | ----- |
| zeroth | first | second | third | fourth | fifth | sixth | seventh | eighth | ninth |



<br>

### 第十到第十九


| 第十  | 第十一   | 第十二  | 第十三     | 第十四     | 第十五    | 第十六    | 第十七      | 第十八     | 第十九     |
| ----- | -------- | ------- | ---------- | ---------- | --------- | --------- | ----------- | ---------- | ---------- |
| tenth | eleventh | twelfth | thirteenth | fourteenth | fifteenth | sixteenth | seventeenth | eighteenth | nineteenth |



<br>

### 第几十，第二十到第九十

> 把基数词结尾的`Y变成i+eth`

| 第二十    | 第三十    | 第四十   | 第五十   | 第六十   | 第七十     | 第八十    | 第九十    |
| --------- | --------- | -------- | -------- | -------- | ---------- | --------- | --------- |
| twentieth | thirtieth | fortieth | fiftieth | sixtieth | seventieth | eightieth | ninetieth |



<br>



### 第二十以上，第几十几

> 十位数用基数词，个位用序数词



```
twenty first 第二十一

ninety ninth 第九十九
```



<br>





### 第一百及以上
| 第一百        | 第一千         | 第一百万      | 第十亿        |
| ------------- | -------------- | ------------- | ------------- |
| one hundredth | one thousandth | one millionth | one billionth |



> 第整数的后面加th,如果不是整数，保留基数词只变个位数

```
one billion fifty million and thirty first
第十亿五千万零三十一 (1,050,000,031 st)
```



<br>



### 序数词常见使用方式



1. 使用序数词时，一般前面要加定冠词the
   `I'm in the second room ( 我在第二个房间)`
2. 表示时间日期
   `The first of September=Sep 1st (九月一日)`
3. 表示楼层
   `The first floor(一楼)`





| 说法 | 三楼(G3)     | 二楼(G2)     | 一楼(G1)     | 地下一层(B1)   | 地下二层(B2)    | 地下三层(B3 )  |
| ---- | ------------ | ------------ | ------------ | -------------- | --------------- | -------------- |
| 英式 | Second floor | First floor  | Ground floor | First basement | Second basement | Third basement |
| 美式 | Third floor  | Second floor | First floor  | First basement | Second basement | Third basement |

<br>



### **序数词表示分数**

one third 

$$
\frac{1}{3}
$$


tow thirds 

$$
\frac{2}{3}
$$


> 序数词可以表示几分之几，分子大于一时，分母要用复数形式。

<br>



### 序数词前面加a/an表示再一

We read the book a third times 
我们读了三遍这本书



<br>



### 序数词前面加the表示顺序



The second door of the room is closed
房间的第二扇门是关的





